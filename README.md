# translator-api
Service used by core-api service, responsible for consuming messages from kafka topic (source data is posted by core-api), uses an external api for translation, and saves in mongoDb translation for each word, and associated user ids, with no duplicates 

## Run local infrastructure( zookeeper, kafka, it's topic "test" and replication factor set to 1 ): ##
> cd local-infrastructure && docker-compose up

## Run cmd producer: ##
> cd local-infrastructure && kafka-producer.sh

## Run cmd a consumer: ##
> cd local-infrastructure && kafka-consumer.sh

## Connect to mongoDb with credentials: ##
> username: admin <br/>
> password: admin <br/>
> host: localhost <br/>
> database: test <br/>
> port: 27017 <br/>
> authentication-database: admin <br/>

## Retrieve data from mongoDb: ##
> test.getCollection('wordDoc').find({}).limit(501)
