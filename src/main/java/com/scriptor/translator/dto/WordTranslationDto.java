package com.scriptor.translator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WordTranslationDto {
    @NotEmpty
    private String lang;
    @NotEmpty
    @JsonProperty(value = "text")
    private List<String> text;
}
