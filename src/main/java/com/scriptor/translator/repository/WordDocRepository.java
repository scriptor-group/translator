package com.scriptor.translator.repository;

import com.scriptor.translator.enitty.WordDoc;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WordDocRepository extends MongoRepository<WordDoc, WordDoc.WordLangKey> {
}
