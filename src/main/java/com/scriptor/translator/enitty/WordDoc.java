package com.scriptor.translator.enitty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Data
@Builder
@Document
public class WordDoc {
    @Id
    private WordLangKey key;
    private Set<String> translations;
    private List<String> userIds;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class WordLangKey implements Serializable {
        private String word;
        private String lang;
    }
}
