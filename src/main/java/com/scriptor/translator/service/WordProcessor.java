package com.scriptor.translator.service;

import com.scriptor.translator.dto.WordTranslationDto;
import com.scriptor.translator.dto.kafka.WordDto;
import com.scriptor.translator.enitty.WordDoc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

@Service
public class WordProcessor {
    private final TranslatorService translatorService;
    private final MongoTemplate mongoTemplate;

    @Autowired
    public WordProcessor(TranslatorService translatorService, MongoTemplate mongoTemplate) {
        this.translatorService = translatorService;
        this.mongoTemplate = mongoTemplate;
    }

    public void process(WordDto dto) {

        WordTranslationDto restResponse = translatorService.getTranslation(dto);

        Query query = new Query();
        query.addCriteria(Criteria.where("_id").exists(true)
                .andOperator(
                        Criteria.where("_id").is(new WordDoc.WordLangKey(dto.getWord(), restResponse.getLang()))));
        Update update = new Update();
        update.set("translations", restResponse.getText());
        update.addToSet("userIds", dto.getUserId());

        mongoTemplate.upsert(query, update, WordDoc.class);
    }
}
