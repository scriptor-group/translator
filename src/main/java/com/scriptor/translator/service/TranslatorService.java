package com.scriptor.translator.service;

import com.scriptor.translator.dto.kafka.WordDto;
import com.scriptor.translator.dto.WordTranslationDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import static java.lang.String.format;


@Service
public class TranslatorService {

    private final RestTemplate restTemplate;
    @Value("${translator.url-key}")
    private String apiKey;
    @Value("${translator.url}")
    private String url;

    public TranslatorService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public WordTranslationDto getTranslation(WordDto dto) {
        return restTemplate.getForEntity(
                format("%s?lang=en-%s&text=%s&key=%s", url, dto.getLang(), dto.getWord(), apiKey),
                WordTranslationDto.class).getBody();

    }
}
