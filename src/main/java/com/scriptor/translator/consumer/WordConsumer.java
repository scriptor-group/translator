package com.scriptor.translator.consumer;

import com.scriptor.translator.dto.kafka.WordDto;
import com.scriptor.translator.service.WordProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class WordConsumer {
    private final WordProcessor wordProcessor;

    public WordConsumer(WordProcessor wordProcessor) {
        this.wordProcessor = wordProcessor;
    }

    @KafkaListener(topics = "${kafka.topic}", groupId = "group1")
    public void consume(@Payload WordDto dto) {
        log.info(dto.toString());
        wordProcessor.process(dto);

    }
}
