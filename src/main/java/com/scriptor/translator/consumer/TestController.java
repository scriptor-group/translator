package com.scriptor.translator.consumer;

import com.scriptor.translator.repository.WordDocRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TestController {

    @Autowired
    private WordDocRepository wordDocRepository;

    @GetMapping("test")
    public void testDebugEndpoint() {
        int i = 0;
    }
}
