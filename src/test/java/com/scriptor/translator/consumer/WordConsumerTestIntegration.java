package com.scriptor.translator.consumer;

import com.scriptor.translator.dto.kafka.WordDto;
import com.scriptor.translator.service.WordProcessor;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.MessageListenerContainer;
import org.springframework.kafka.support.SendResult;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Map;

import static com.scriptor.translator.consumer.WordConsumerTestIntegration.TOPIC;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@EmbeddedKafka(partitions = 1, topics = TOPIC,
        bootstrapServersProperty = "spring.kafka.bootstrap-servers")
@DirtiesContext
public class WordConsumerTestIntegration {
    public static final String TOPIC = "test";

    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private EmbeddedKafkaBroker embeddedKafka;

    public static final String USER_ID = "userId";
    public static final String LANG = "ru";
    public static final String WORD = "word";
    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private KafkaListenerEndpointRegistry kafkaListenerEndpointRegistry;

    @MockBean
    private WordProcessor wordProcessor;

    @Test
    void consumeMessages_success() throws Exception {
        Map<String, Object> configs = KafkaTestUtils.producerProps(embeddedKafka);
        configs.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configs.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        ProducerFactory<String, WordDto> pf =
                new DefaultKafkaProducerFactory<>(configs,
                        new StringSerializer(),
                        new JsonSerializer<>());
        KafkaTemplate<String, WordDto> template = new KafkaTemplate<>(pf);
        template.setDefaultTopic(TOPIC);

        for (MessageListenerContainer messageListenerContainer : kafkaListenerEndpointRegistry
                .getListenerContainers()) {
            ContainerTestUtils.waitForAssignment(messageListenerContainer, embeddedKafka.getPartitionsPerTopic());
        }

        WordDto wordDto = WordDto.builder().userId(USER_ID).lang(LANG).word(WORD).build();
        SendResult<String, WordDto> stringWordDtoSendResult = template.sendDefault(wordDto).get();

        assertNotNull(stringWordDtoSendResult);
        verify(wordProcessor, timeout(10000).times(1)).process(wordDto);

    }
}

