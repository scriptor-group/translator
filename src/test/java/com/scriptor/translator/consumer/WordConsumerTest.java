package com.scriptor.translator.consumer;

import com.scriptor.translator.dto.kafka.WordDto;
import com.scriptor.translator.service.WordProcessor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class WordConsumerTest {
    @InjectMocks
    private WordConsumer wordConsumer;
    @Mock
    private WordProcessor wordProcessor;

    @Test
    void listen_successful() {
        WordDto dto = WordDto.builder().userId("1").lang("lang").word("word").build();
        wordConsumer.consume(dto);
        verify(wordProcessor).process(dto);
    }
}
