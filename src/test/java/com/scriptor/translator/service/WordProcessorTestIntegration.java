package com.scriptor.translator.service;

import com.scriptor.translator.dto.WordTranslationDto;
import com.scriptor.translator.dto.kafka.WordDto;
import com.scriptor.translator.enitty.WordDoc;
import com.scriptor.translator.repository.WordDocRepository;
import org.apache.commons.compress.utils.Sets;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@DataMongoTest
@ExtendWith(SpringExtension.class)
@DirtiesContext
@TestPropertySource(
        properties = {
                "spring.data.mongodb.username:",
                "spring.data.mongodb.password:",
                "spring.data.mongodb.host:",
                "spring.data.mongodb.database: test",
                "spring.data.mongodb.port:",
                "spring.data.mongodb.authentication-database:"
        }
)
class WordProcessorTestIntegration {
    public static final String LANG = "lang";
    public static final String USER_ID = "1";
    public static final String USER_ID_2 = "2";
    public static final String WORD = "word";
    public static final String TRANSLATION_1 = "translation1";
    public static final String TRANSLATION_2 = "translation2";
    public static final String TRANSLATION_3 = "translation3";

    @MockBean
    private TranslatorService translatorService;

    @Autowired
    private WordDocRepository wordDocRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    void doTranslate_saveOneSuccessful() {
        WordProcessor wordProcessor = new WordProcessor(translatorService, mongoTemplate);

        WordDto dto = WordDto.builder().userId(USER_ID).lang(LANG).word(WORD).build();
        WordTranslationDto translatorResponse = WordTranslationDto
                .builder()
                .lang(LANG)
                .text(Arrays.asList(TRANSLATION_1, TRANSLATION_2))
                .build();
        when(translatorService.getTranslation(dto)).thenReturn(translatorResponse);

        wordProcessor.process(dto);

        Optional<WordDoc> actualWord = wordDocRepository.findById(new WordDoc.WordLangKey(WORD, LANG));
        assertTrue(actualWord.isPresent());
        assertEquals(Sets.newHashSet(TRANSLATION_1, TRANSLATION_2), actualWord.get().getTranslations());
        assertEquals(Collections.singletonList(USER_ID), actualWord.get().getUserIds());
    }

    @Test
    void doTranslate_updateOneSuccessful() {

        WordDoc presentWordDoc = WordDoc.builder()
                .key(new WordDoc.WordLangKey(WORD, LANG))
                .userIds(Collections.singletonList(USER_ID_2))
                .translations(Sets.newHashSet(TRANSLATION_1, TRANSLATION_2))
                .build();
        wordDocRepository.save(presentWordDoc);


        WordProcessor wordProcessor = new WordProcessor(translatorService, mongoTemplate);
        WordDto dto = WordDto.builder().userId(USER_ID).lang(LANG).word(WORD).build();
        WordTranslationDto translatorResponse = WordTranslationDto
                .builder()
                .lang(LANG)
                .text(Arrays.asList(TRANSLATION_1, TRANSLATION_2, TRANSLATION_3))
                .build();
        when(translatorService.getTranslation(dto)).thenReturn(translatorResponse);

        wordProcessor.process(dto);

        Optional<WordDoc> actualWord = wordDocRepository.findById(new WordDoc.WordLangKey(WORD, LANG));
        assertTrue(actualWord.isPresent());
        assertEquals(Sets.newHashSet(TRANSLATION_1, TRANSLATION_2, TRANSLATION_3), actualWord.get().getTranslations());
        assertEquals(Arrays.asList(USER_ID_2, USER_ID), actualWord.get().getUserIds());
    }

    @Test
    void doTranslate_updateNotPushIfAlreadyPresent() {

        WordDoc presentWordDoc = WordDoc.builder()
                .key(new WordDoc.WordLangKey(WORD, LANG))
                .userIds(Collections.singletonList(USER_ID))
                .translations(Sets.newHashSet(TRANSLATION_1, TRANSLATION_2))
                .build();
        wordDocRepository.save(presentWordDoc);

        WordProcessor wordProcessor = new WordProcessor(translatorService, mongoTemplate);
        WordDto dto = WordDto.builder().userId(USER_ID).lang(LANG).word(WORD).build();
        WordTranslationDto translatorResponse = WordTranslationDto
                .builder()
                .lang(LANG)
                .text(Arrays.asList(TRANSLATION_1, TRANSLATION_2, TRANSLATION_3))
                .build();
        when(translatorService.getTranslation(dto)).thenReturn(translatorResponse);

        wordProcessor.process(dto);

        Optional<WordDoc> actualWord = wordDocRepository.findById(new WordDoc.WordLangKey(WORD, LANG));
        assertTrue(actualWord.isPresent());
        assertEquals(Sets.newHashSet(TRANSLATION_1, TRANSLATION_2, TRANSLATION_3), actualWord.get().getTranslations());
        assertEquals(Collections.singletonList(USER_ID), actualWord.get().getUserIds());
    }
}
