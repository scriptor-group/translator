package com.scriptor.translator.service;

import com.scriptor.translator.dto.kafka.WordDto;
import com.scriptor.translator.dto.WordTranslationDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TranslatorServiceTest {
    public static final String API_KEY_VALUE = "apiKeyValue";
    public static final String URL_VALUE = "urlValue";
    public static final String LANG = "lang";
    public static final String USER_ID = "1";
    public static final String WORD = "word";
    public static final String TRANSLATION_1 = "translation1";
    public static final String TRANSLATION_2 = "translation2";
    @InjectMocks
    private TranslatorService translatorService;
    @Mock
    private RestTemplate restTemplate;

    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(translatorService, "apiKey", API_KEY_VALUE);
        ReflectionTestUtils.setField(translatorService, "url", URL_VALUE);
    }

    @Test
    void doTranslate_successful() {
        WordDto dto = WordDto.builder().userId(USER_ID).lang(LANG).word(WORD).build();

        String expectedUrl = String.format("%s?lang=en-%s&text=%s&key=%s", URL_VALUE, LANG, WORD, API_KEY_VALUE);
        WordTranslationDto wordTranslationDto = WordTranslationDto
                .builder()
                .lang(LANG)
                .text(Arrays.asList(TRANSLATION_1, TRANSLATION_2))
                .build();
        ResponseEntity<WordTranslationDto> restTranslatedResponse = new ResponseEntity<>(wordTranslationDto, HttpStatus.OK);

        when(restTemplate.getForEntity(expectedUrl, WordTranslationDto.class)).thenReturn(restTranslatedResponse);

        WordTranslationDto translation = translatorService.getTranslation(dto);

        assertEquals(wordTranslationDto, translation);
    }
}
