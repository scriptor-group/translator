package com.scriptor.translator.service;

import com.scriptor.translator.dto.WordTranslationDto;
import com.scriptor.translator.dto.kafka.WordDto;
import com.scriptor.translator.enitty.WordDoc;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.Arrays;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WordProcessorTest {
    public static final String LANG = "lang";
    public static final String USER_ID = "1";
    public static final String WORD = "word";
    public static final String TRANSLATION_1 = "translation1";
    public static final String TRANSLATION_2 = "translation2";
    @InjectMocks
    private WordProcessor wordProcessor;
    @Mock
    private TranslatorService translatorService;
    @Mock
    private MongoTemplate mongoTemplate;

    @Test
    void doTranslate_successful() {
        WordDto dto = WordDto.builder().userId(USER_ID).lang(LANG).word(WORD).build();

        when(translatorService.getTranslation(dto)).thenReturn(WordTranslationDto
                .builder()
                .lang(LANG)
                .text(Arrays.asList(TRANSLATION_1, TRANSLATION_2))
                .build());

        wordProcessor.process(dto);
        Query expectedQuery = new Query();
        expectedQuery.addCriteria(Criteria.where("_id").exists(true)
                .andOperator(
                        Criteria.where("_id").is(new WordDoc.WordLangKey(dto.getWord(), LANG))));
        Update expectedUpdate = new Update();
        expectedUpdate.set("translations", Arrays.asList(TRANSLATION_1, TRANSLATION_2));
        expectedUpdate.addToSet("userIds", dto.getUserId());

        verify(mongoTemplate).upsert(expectedQuery, expectedUpdate, WordDoc.class);
    }
}
