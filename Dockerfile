FROM openjdk:8 AS build
COPY src /usr/src/app/src
COPY build.gradle /usr/src/app
COPY settings.gradle /usr/src/app
COPY gradlew /usr/src/app
COPY gradlew.bat /usr/src/app
COPY gradle /usr/src/app/gradle
RUN /usr/src/app/gradlew -p /usr/src/app clean build

FROM openjdk:8-jdk-alpine
COPY --from=build /usr/src/app/build/libs/translator-0.0.1-SNAPSHOT.jar /usr/app/app.jar
EXPOSE 8082 5005
ENV JAVA_TOOL_OPTIONS -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005
ENTRYPOINT ["java","-jar","/usr/app/app.jar"]

#docker run -i -t _ /bin/sh
